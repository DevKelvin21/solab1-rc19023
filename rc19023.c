#include <linux/kernel.h>
#include <sys/syscall.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>

#define __NR_idcall 451

long id_syscall(void)
{
	return syscall(__NR_idcall);
}

int test_syscall(void)
{
	long activity;
	activity = id_syscall();
	if(activity < 0)
	{
		perror("Sorry, Syscall failed");
	}
	else
	{
		printf("Success, Syscall works!\n");
	}
	return 0;

}
